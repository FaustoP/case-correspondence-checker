﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TheCherington
{
    class Comparer
    {
        public Dictionary<string, Tuple<bool, bool> >ComparerResults = new Dictionary<string, Tuple<bool, bool>>();   
        public Comparer(Dictionary<string, CubsCaseObject> subSelection , IEnumerable<string> fedFeeder)
        {
            foreach (var iCase in fedFeeder)
            {
                if (!ComparerResults.ContainsKey(iCase))
                {
                    if (subSelection.ContainsKey(iCase))
                    {
                        var tuple = new Tuple<bool, bool>(true,
                            subSelection[iCase].EmptyDirectory);
                        ComparerResults.Add(iCase, tuple);
                    }
                    else
                    {
                        var tuple = new Tuple<bool, bool>(false, false);
                        ComparerResults.Add(iCase, tuple);
                    }
                }
            }
            
        }
    }
}
