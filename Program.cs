﻿using System;

namespace TheCherington
{
    internal class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            Console.WindowWidth = Console.LargestWindowWidth;
            Console.WindowHeight = Console.LargestWindowHeight;
            GreetForFun();
            do
            {
                Run();
            } while (true);


        }

        private static void Run()
        {
            Console.WriteLine("\n\rThe Cherington Program is ready to do a correspondence check. Note this version does Concurrent Processing.");


            string input = string.Empty;
            bool proceed = false;

            do
            {
                Console.WriteLine("Type out the exact kind of case you want to search for, example: HRA2 or NYPD1");
                input = Console.ReadLine();
                Console.WriteLine(
                    "You're positive that {0} is the type of case you want to search for? Type y for Yes... Or n for No",
                    input);
                var adminSayproceed = Console.ReadLine();
                if (adminSayproceed.ToLower() == "y")
                {
                    proceed = true;
                }
            } while (!proceed);
            Mapper newMap = new Mapper(input.ToUpper());
            Feeder myFeeder = new Feeder();
            Comparer comparison = new Comparer(newMap.Database, myFeeder.fedTasks);
            ExcelOutputter excelRelease = new ExcelOutputter(comparison.ComparerResults, input.ToUpper());
        }

        public static void GreetForFun()
        {
            var arr = new[]
            {
                @"                   ________  __                         ______   __                            __                       __         ",
                @"  |        \|  \                       /      \ |  \                          |  \                     |  \                        ",
                @"  \$$$$$$$$| $$____    ______        |  $$$$$$\| $$____    ______    ______   \$$ _______    ______  _| $$_     ______   _______  ",
                @"   | $$   | $$    \  /      \       | $$   \$$| $$    \  /      \  /      \ |  \|       \  /      \|   $$ \   /      \ |       \ ",
                @"  | $$   | $$$$$$$\|  $$$$$$\      | $$      | $$$$$$$\|  $$$$$$\|  $$$$$$\| $$| $$$$$$$\|  $$$$$$\\$$$$$$  |  $$$$$$\| $$$$$$$\",
                @" | $$   | $$  | $$| $$    $$      | $$   __ | $$  | $$| $$    $$| $$   \$$| $$| $$  | $$| $$  | $$ | $$ __ | $$  | $$| $$  | $$",
                @" | $$   | $$  | $$| $$$$$$$$      | $$__/  \| $$  | $$| $$$$$$$$| $$      | $$| $$  | $$| $$__| $$ | $$|  \| $$__/ $$| $$  | $$",
                @"| $$   | $$  | $$ \$$     \       \$$    $$| $$  | $$ \$$     \| $$      | $$| $$  | $$ \$$    $$  \$$  $$ \$$    $$| $$  | $$",
                @" \$$    \$$   \$$  \$$$$$$$        \$$$$$$  \$$   \$$  \$$$$$$$ \$$       \$$ \$$   \$$ _\$$$$$$$   \$$$$   \$$$$$$  \$$   \$$",
                @"                                                                                      |  \__| $$                             ",
                @"                                                                                       \$$    $$                             ",
                @"                                                                                        \$$$$$$                              ",
            };
            Console.WriteLine("\n\n");
            foreach (string line in arr)
                Console.WriteLine(line);
        }
    }
}