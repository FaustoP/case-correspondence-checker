﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Forms;

/* The purpose of this class is to run on start-up of the program, and properly map out the directories
 * whilst keeping this information in memory, stored in a dictionary within an object
 * the dictionary will contain the key - cubs number, and the value - the object containing the file path information
 */

namespace TheCherington
{
    internal class Mapper
    {
        public const string Rootfolder = @"\\itgcubs\itgcubs$\prodcubsdata\FF_Data\CubsImages\NYLit\Debtor Files\CUBS\";
        public Dictionary<string, CubsCaseObject> Database = new Dictionary<string, CubsCaseObject>();
        public ConcurrentDictionary<string, CubsCaseObject> ConDictionary = new ConcurrentDictionary<string, CubsCaseObject>(); 

        public Mapper(string focus)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            Console.WriteLine("We're going to be focusing on cases of category type {0}", focus);
            Console.WriteLine("Mapping out the directories beginning at the root folder: {0} \n", Rootfolder);
            // Input all the sub-folders within the rootfolder in this array, so we can analyze them one at a time.
            var directories = Directory.GetDirectories(Rootfolder);


            // For each sub directory found in our root directory..
            foreach (var dir in directories)
            {

                if (dir.Contains(focus))
                {
                    Console.WriteLine("Currently Mapping directory: {0} \n", dir);
                    //Now we're mapping all the cubs folders within each sub directory.
                    var subdirectories = Directory.GetDirectories(dir);
                    var count = 1;
                    Parallel.ForEach(subdirectories, currentDir =>
                    {
                        
                        var cubsNumber = new DirectoryInfo(currentDir).Name;
                        var currentCase = new CubsCaseObject(currentDir);
                        try
                        {
                            //Simply try to add the key and Cubs Object to the dictionary.
                            Database.Add(cubsNumber, currentCase);
                            var completion = (count*100)/subdirectories.Length;
                            Console.Write("\r Adding Case {0}. Progress towards completion: {1}%", cubsNumber,
                                completion);
                            count++;
                        }
                        catch (Exception ShouldNotOccur)
                        {
                            Console.WriteLine("This exception should not occur: \n {0}", ShouldNotOccur);
                        }
                    });
                }
            }
            //This just stops the watch, as I was running it for diagnostics. The last Console statement here just prints the elapsed time.
            stopWatch.Stop();
            Console.WriteLine("\nTOTAL TIME REQUIRED FOR MAPPING \n");
            var ts = stopWatch.Elapsed;
            var elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00} \n",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds/10);
            Console.WriteLine(elapsedTime);
        }
    }
}