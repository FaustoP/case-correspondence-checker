﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace TheCherington
{
    internal class Feeder
    {
       public List<string> fedTasks = new List<string>();  
        
        public Feeder()
        {
            Console.WriteLine("Please select the input text file.\n");
            var feederSelect = new OpenFileDialog
            {
                InitialDirectory = @"c:\",
                Filter = "Text|*.txt|All|*.*"
            };
            feederSelect.ShowDialog();

            var file = feederSelect.FileName;
            using (StreamReader reader = new StreamReader(file))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    fedTasks.Add(line);
                    Console.Write("\rReading from file: {0}", line);
                }
            }
            Console.WriteLine("\n\rFile Reading complete, fed tasks accounted for, there are {0} entries", fedTasks.Count);
        }
    }
}
