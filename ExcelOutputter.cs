﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;


namespace TheCherington
{
    class ExcelOutputter
    {
        public ExcelOutputter(Dictionary<string, Tuple<bool, bool>> finalResultsDictionary, string caseType)
        {
            var xlApp = new Microsoft.Office.Interop.Excel.Application();
            Excel.Workbook xlWorkbook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlWorkbook = xlApp.Workbooks.Add((misValue));
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet) xlWorkbook.Worksheets.get_Item(1);
            int count = 2;
            xlWorkSheet.Cells[1, 1] = "CUBS NUMBER";
            xlWorkSheet.Cells[1, 2] = "XMG Folder Exists";
            xlWorkSheet.Cells[1, 3] = "XMG Folder Empty";
            int progress = 1;
            foreach (var entry in finalResultsDictionary)
            {
                var completion = (progress*100)/finalResultsDictionary.Count;
                Console.Write("\rWriting case {0} to Excel. Progress to Completion: {1}%", entry.Key, completion);
                
                xlWorkSheet.Cells[count, 1] = entry.Key;
                xlWorkSheet.Cells[count, 2] = EvaluateXMGExists(entry.Value.Item1);
                if(entry.Value.Item1)
                {xlWorkSheet.Cells[count, 3] = EvaluateFolderEmpty(entry.Value.Item2);}
                count++;
                progress++;
            }
            Console.WriteLine("Please select save location using the file-save dialog.");
            try
            {
                string excelOutputFilePath = requestSaveLocation() + @"\" + NameExcel(caseType, finalResultsDictionary.Count.ToString());
                xlWorkbook.SaveAs(
                    string.Format(excelOutputFilePath, DateTime.Now.ToString(CultureInfo.InvariantCulture)),
                    Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue,
                    Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkbook.Close(true, misValue, misValue);
                xlApp.Quit();

                ReleaseObject(xlWorkSheet);
                ReleaseObject(xlWorkbook);
                ReleaseObject(xlApp);
                Console.WriteLine("\n"+ @"Excel file was created, take a look at {0}", excelOutputFilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n\r We have run into an exception \n\r: {0}", e);
            }

        }

        private string requestSaveLocation()
        {
            var saveDialog = new FolderBrowserDialog();
            var result = saveDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                var folderName = saveDialog.SelectedPath;
                return folderName;
            }
            else
            {
                Console.WriteLine("You did not select ok so it is being saved to your C Drive.");
                return @"C:\";
            }
            


        }

        private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                Console.WriteLine("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private string NameExcel(string caseType, string count)
        {
            string result = string.Format(@"{0}-XMG Records-{1} Cases({2}).xls", caseType, count, DateTime.Now.ToString("M-dd-yy"));
            return result;
        }

        private string EvaluateFolderEmpty(bool EmptyFolder)
        {
            if (EmptyFolder)
            {
                return "Vacant";
            }
            else
            {
                return "Occupied";
            }
        }

        private string EvaluateXMGExists(bool xmgExists)
        {
            if (xmgExists)
            {
                return "Exists";
            }
            else
            {
                return "Non-existent";
            }
        }
    }
}
